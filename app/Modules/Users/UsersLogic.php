<?php  
namespace App\Modules\Users;

use Illuminate\Http\Request;
use App\Modules\Users\UsersBaseLogic as BaseLogic;

	class UsersLogic{
		protected $baseLogic;
		protected $request;

		public function __construct(Request $request, BaseLogic $baseLogic){
			$this->baseLogic=$baseLogic;
			$this->request=$request;
		}

		public function users(){
			$users=$this->baseLogic->getUsers();
			return $users;
		}
	}

?>