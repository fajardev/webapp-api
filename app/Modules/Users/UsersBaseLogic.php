<?php  
namespace App\Modules\Users;

use Illuminate\Http\Request;
use App\Models\UsersModel;

	class UsersBaseLogic {

		protected $users;

		public function __construct(UsersModel $users){
			$this->users=$users;
		}

		public function getUsers(){
			$users=$this->users->all();
			return $users;
		}
	}

?>